import QuestionItem from "../core/QuestionItem";
import AssessmentForm from "../core/AssessmentForm";

export const pms1Test = generatePms1Test();
export function generatePms1Test() {
  return new AssessmentForm({
    id: 1,
    title: "PMS 1",
    questionItems: [
      new QuestionItem({
        question: "What is the ultimate mission of FICCO?",
        correctAnswer: "To uplift the lives of our members.",
        options: [
          "To uplift the lives of our members.",
          "To uplift the spirit.",
          "To gain more income.",
        ],
      }),
      new QuestionItem({
        question: "What are the three personalities of FICCO?",
        correctAnswer:
          "As a cooperative, as a business enterprise, as a development catalyst.",
        options: [
          "As a cooperative, as a business enterprise, as a development catalyst.",
          "As a cooperative, as a charity, as a catalyst.",
          "As an enterprise, as a lending, as an educator.",
        ],
      }),
      new QuestionItem({
        question: "Which is the correct FICCO formula?",
        correctAnswer: "Income - Savings = Expense",
        options: [
          "Income - Savings = Expense",
          "Income - Expense = Savings",
          "Savings - Expense = Income",
        ],
      }),
      new QuestionItem({
        question: "Which of the following describes a development catalyst?",
        correctAnswer: "Help myself to help others.",
        options: [
          "Help myself to help others.",
          "Borrow from FICCO and lend to others.",
          "Help others ",
        ],
      }),
      new QuestionItem({
        question: "When did FICCO started?",
        correctAnswer: "1954",
        options: ["1954", "1964", "1944"],
      }),
    ],
  });
}

export const pms2Test = generatePms2Test();
export function generatePms2Test() {
  return new AssessmentForm({
    id: 2,
    title: "PMS 2",
    questionItems: [
      new QuestionItem({
        question: "Who is the current CEO of FICCO?",
        correctAnswer: "Edgardo A. Micayabas",
        options: [
          "Wilfredo G. Samuya",
          "Ernie A. Obsina",
          "Edgardo A. Micayabas",
        ],
      }),
      new QuestionItem({
        question: "Who has the highest authority in FICCO?",
        correctAnswer: "General Assembly",
        options: ["BOD", "General Assembly", "CEO"],
      }),
      new QuestionItem({
        question: "What makes FICCO services price competitive?",
        correctAnswer: "Volunteers",
        options: ["Consultants", "Managers", "Volunteers"],
      }),
      new QuestionItem({
        question: "How much do you need to become a regular member?",
        correctAnswer: "PHP 3,500.00",
        options: ["PHP 3,500.00", "PHP 500.00", "PHP 3,000.00"],
      }),
      new QuestionItem({
        question: "What do you get as a FICCO member?",
        correctAnswer: "Dividend and patronage refund",
        options: [
          "Interest and savings",
          "Dividend and patronage refund",
          "Loan and cash",
        ],
      }),
    ],
  });
}

export const pms3Test = generatePms3Test();
export function generatePms3Test() {
  return new AssessmentForm({
    id: 3,
    title: "PMS 3",
    questionItems: [
      new QuestionItem({
        question: "When can you avail your first loan?",
        correctAnswer:
          "Immediately after completing 3,000.00 share and 500.00 savings",
        options: [
          "Immediately after completing 3,000.00 share and 500.00 savings",
          "After the premembership seminar",
          "After 3 months",
        ],
      }),
      new QuestionItem({
        question: "When can you avail your first regular loan?",
        correctAnswer: "3 months after becoming a regular member",
        options: [
          "Immediately after seminar",
          "3 months after becoming a regular member",
          "After paying first loan",
        ],
      }),
      new QuestionItem({
        question: "LAD, pettycash, and rice loans are considered ___________",
        correctAnswer: "Over the counter loans, no loan briefing needed",
        options: [
          "Over the counter loans, no seminar needed",
          "Regular loans needs loan briefing",
          "Over the counter loans, no loan briefing needed",
        ],
      }),
      new QuestionItem({
        question: "How much is your first maximum regular loan?",
        correctAnswer: "PHP 50,000.00",
        options: ["PHP 1,000,000.00", "PHP 50,000.00", "PHP 100,000.00"],
      }),
      new QuestionItem({
        question: "Which of the following statement is true?",
        correctAnswer: "No loan releases with incomplete requirements",
        options: [
          "No loan releases with incomplete requirements",
          "Loans with incomplete requirements are considered",
          "Only ages below 18 can avail loans",
        ],
      }),
    ],
  });
}

export function generateAssessmentForms() {
  return [generatePms1Test(), generatePms2Test(), generatePms3Test()];
}
