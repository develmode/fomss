import User from "@/core/User";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentUser: new User(),
  },
  mutations: {
    updateUser({ currentUser }, user: User) {
      currentUser.initialize(user);
    },
  },
  actions: {},
});
