import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import RegistrationPage from "../views/RegistrationPage.vue";
import LandingPage from "../views/LandingPage.vue";
import HomePage from "../views/HomePage.vue";
import AssessmentPage from "../views/AssessmentPage.vue";
import SeminarSchedulingPage from "../views/SeminarSchedulingPage.vue";
import CertificationPage from "../views/CertificationPage.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    component: LandingPage,
  },
  {
    path: "/home",
    component: HomePage,
  },
  {
    path: "/assessment/:id",
    component: AssessmentPage,
  },
  {
    path: "/register",
    component: RegistrationPage,
  },
  {
    path: "/schedule-seminar",
    component: SeminarSchedulingPage,
  },
  {
    path: "/certification",
    component: CertificationPage,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
