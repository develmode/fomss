export enum Gender {
  Male = "Male",
  Female = "Female",
}

function values() {
  const genders = [];
  for (let g in Gender) {
    if (isNaN(Number(g))) {
      genders.push(g);
    }
  }
  return genders;
}

export const genderValues = values();
export default Gender;
