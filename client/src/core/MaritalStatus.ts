export enum MaritalStatus {
  Single = "Single",
  Married = "Married",
  Widowed = "Widowed",
  Divorced = "Divorced",
}

export const maritalStatusValues = (() => {
  const status = [];
  for (let s in MaritalStatus) {
    if (isNaN(Number(s))) {
      status.push(s);
    }
  }
  return status;
})();

export default MaritalStatus;
