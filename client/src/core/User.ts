import moment from "moment";
import AssessmentForm from "./AssessmentForm";
import Gender from "./Gender";
import MaritalStatus from "./MaritalStatus";

export default class User {
  firstName = "";
  middleName = "";
  lastName = "";
  extensionName = "";

  address = "";

  birthDate = new Date();

  gender = Gender.Male;

  maritalStatus = MaritalStatus.Single;

  contactNumber = "";

  email = "";
  password = "";
  passwordHash = "";

  ficcoBranch = "";

  assessmentForms = [] as AssessmentForm[];

  paid = false;

  constructor(init?: any) {
    if (init) {
      this.initialize(init);
    }
  }

  public initialize(init: any) {
    const {
      ficcoBranch,
      firstName,
      middleName,
      lastName,
      extensionName,
      address,
      birthDate,
      gender,
      maritalStatus,
      contactNumber,
      email,
      password,
      passwordHash,
      assessmentForms,
      paid,
    } = init;

    this.ficcoBranch = ficcoBranch || "";
    this.firstName = firstName || "";
    this.middleName = middleName || "";
    this.lastName = lastName || "";
    this.extensionName = extensionName || "";
    this.address = address || "";
    this.birthDate = birthDate || this.birthDate;
    this.gender = gender || this.gender;
    this.maritalStatus = maritalStatus || this.maritalStatus;
    this.contactNumber = contactNumber || this.contactNumber;
    this.email = email || "";
    this.password = password || "";
    this.passwordHash = passwordHash || this.passwordHash;
    this.assessmentForms = assessmentForms || this.assessmentForms;
    this.paid = paid || this.paid;
  }

  get firstAndLastNameInitials() {
    return this.firstName.charAt(0) + this.lastName.charAt(0);
  }

  get fullName() {
    return `${this.firstName} ${this.middleName} ${this.lastName} ${this.extensionName}`;
  }
}
