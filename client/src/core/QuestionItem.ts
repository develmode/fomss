export default class QuestionItem {
  question = "";

  options: string[] = [];
  correctAnswer = "";

  answer = "";

  expectedPoint = 1;

  constructor(init?: any) {
    if (!init) return;

    this.initialize(init);
  }

  private initialize(init: any) {
    this.question = init.question;
    this.options = init.options;
    this.correctAnswer = init.correctAnswer;
    this.answer = init.answer;
  }

  get point() {
    return this.answer === this.correctAnswer ? this.expectedPoint : 0;
  }
}
