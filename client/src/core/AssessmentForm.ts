import QuestionItem from "./QuestionItem";

export default class AssessmentForm {
  id = 0;

  title = "";

  questionItems: QuestionItem[] = [];

  constructor(init?: any) {
    if (!init) {
      ++this.id;
      return;
    }
    this.initialize(init);
  }

  private initialize(init: any) {
    this.id = init.id;
    this.title = init.title;
    this.questionItems = init.questionItems;
  }

  get actualPoints() {
    let points = 0;
    for (let qi of this.questionItems) {
      points = points + qi.point;
    }
    return points;
  }

  get totalPoints() {
    let points = 0;
    for (let qi of this.questionItems) {
      points = points + qi.expectedPoint;
    }
    return points;
  }

  get passed() {
    return this.actualPoints === this.totalPoints;
  }

  get scorePercentage() {
    return (this.actualPoints * 100) / this.totalPoints;
  }
}
