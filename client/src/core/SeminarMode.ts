enum SeminarMode {
  Online = "Online",
  Onsite = "Onsite",
}

export default SeminarMode;
