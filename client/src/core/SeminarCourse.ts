export default class SeminarCourse {
  title = "";
  description = "";

  videoThumbnail = "";
  videoUrl = "";

  assessmentFormId = 0;

  passed = false;

  constructor(init?: any) {
    if (!init) return;

    this.initialize(init);
  }

  private initialize(init: any) {
    const {
      title,
      description,
      videoThumbnail,
      videoUrl,
      assessmentFormId,
      passed,
    } = init;
    this.title = title || "";
    this.description = description || "";
    this.videoThumbnail = videoThumbnail || "";
    this.videoUrl = videoUrl || "";
    this.assessmentFormId = assessmentFormId || 0;
    this.passed = !!passed;
  }
}
