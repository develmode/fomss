# fomss

## Pre-requisites:

- Node JS v12.14.1 and up. [Download here.](https://nodejs.org/en/download/)
- Yarn. [Download here for windows](https://classic.yarnpkg.com/en/docs/install/#windows-stable)

## Setup

After cloning the project using `git clone *<url_for_this_project>*`, navigate to the `client` folder, like so:

> `cd client`

and under the `client` folder, type in this command:

> `yarn`

`yarn` will going to install the required dependency for this project. This step requires an internet connection for the deps to be downloaded. Depending on your internet speed the installation may take a while.

After `yarn` succeeds, you may now type in the following command to run the dev server, serving FICCO's pre-registration web app.

> `yarn serve`

If you want an optimal build for the web app for production deployment, use this command:

> `yarn build`

## Command Summary

> `git clone *<url_for_this_project>*`

> `cd client`

> `yarn`

> `yarn serve`

Optionally:

> `yarn build`
